gsap.set('.content-container', {scale: 1});

gsap
  .timeline()
  .from('.nav-bar', {opacity: 0, ease: 'back', duration: 1, y: 150})
  .from('.footer', {opacity: 0, ease: 'back', duration: 1, y: -150})
  .from('.bg-image', {opacity: 0, ease: 'back', duration: 1, x: -150})
  .from('.bg-image-left', {opacity: 0, ease: 'back', duration: 1, x: 150})
  .from('.contact', {opacity: 0, ease: 'back', duration: 1, y: -150})
  .from('.fb', {opacity: 0, ease: 'back', duration: 1, x: -150})
  .from('.linkedin', {opacity: 0, ease: 'back', duration: 1, x: 150})
  .from('.about-me', {opacity: 0, ease: 'back', duration: 1, y: -150})
  .from('.info', {opacity: 0, ease: 'back', duration: 1, y: 150});
